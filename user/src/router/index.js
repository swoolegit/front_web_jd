import Vue from 'vue'
import Router from 'vue-router'

import UserLayout from '@/components/layouts/User'
import DefaultLayout from '@/components/layouts/Default'
import HelpLayout from '@/components/layouts/Help'


import Index from '@/components/Index'
//import Live from '@/components/Live'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)
import { mapGetters } from 'vuex'
import axios from 'axios'

const router = new Router({
	//mode: 'history',
	routes: [
		{
		    path: '/user',
		    name: 'UserLayout',
		    component: UserLayout,
		    children: [
				{
				    path: 'wallet',
				    name: '資產總覽',
				    component: () => import('@/components/views/user/Wallet')
				},
				{
				    path: 'deposit',
				    name: '存款專區',
				    component: () => import('@/components/views/user/Deposit')
				},
				{
				    path: 'withdraw',
				    name: '提款專區',
				    component: () => import('@/components/views/user/Withdraw')
				},
				{
				    path: 'transfer-log',
				    name: '資金記錄',
				    component: () => import('@/components/views/user/TransferLog')
				},
				{
				    path: 'bet-log',
				    name: '投注記錄',
				    component: () => import('@/components/views/user/BetLog')
				},
				{
				    path: 'info',
				    name: '個人資料',
				    component: () => import('@/components/views/user/Info')
				},
				{
				    path: 'mail',
				    name: '站內信',
				    component: () => import('@/components/views/user/Mail')
				},
				{
				    path: 'password',
				    name: '修改登錄密碼',
				    component: () => import('@/components/views/user/Password')
				},
				{
				    path: 'email',
				    name: '個人信箱',
				    component: () => import('@/components/views/user/Email')
				},
				{
				    path: 'cash-password',
				    name: '設置提款密碼',
				    component: () => import('@/components/views/user/CashPassword')
				},
				{
				    path: 'reset-cashpassword',
				    name: '重置提款密碼',
				    component: () => import('@/components/views/user/ResetCashpassword')
				},
				{

				    path: 'mobile',
				    name: '設置手機號碼',
				    component: () => import('@/components/views/user/Mobile')
				},
				{
				    path: 'set-bank',
				    name: '設定銀行資料',
				    component: () => import('@/components/views/user/SetBank')
				},



			]
		},
		{
			path: '/',
			component: DefaultLayout,
			children: [
				{
				    path: '',
				    name: '首頁',
				    component: Index
				},
				{
				    path: 'live',
				    name: '真人娛樂',
				    component: () => import('@/components/views/Live')
				},
				{
				    path: 'chess',
				    name: '棋牌對戰',
				    component: () => import('@/components/views/Chess')
				},
				{
				    path: 'fisher',
				    name: '捕魚達人',
				    component: () => import('@/components/views/Fisher')
				},
				{
				    path: 'av/:action?',
				    name: 'AV影城',
				    component: () => import('@/components/views/AV')
				},
				{
				    path: 'sport',
				    name: '體育博彩',
				    component: () => import('@/components/views/Sport')
				},
				{
				    path: 'slot',
				    name: '電子遊戲',
				    component: () => import('@/components/views/Slot')
				},
				{
				    path: 'slot/qt',
				    name: 'QT電子',
				    component: () => import('@/components/views/SlotQT')
				},
				{
				    path: 'sport/qt',
				    name: 'QT虛擬體育',
				    component: () => import('@/components/views/SportQT')
				},
				{
				    path: 'slot/ae',
				    name: 'AE電子',
				    component: () => import('@/components/views/SlotAE')
				},
				{
				    path: 'slot/sa',
				    name: 'SA電子',
				    component: () => import('@/components/views/SlotSA')
				},
				{
					path: 'slot/ttg',
					name: 'TTG電子',
					component: () => import('@/components/views/SlotTTG')
				},
				{
					path: 'slot/rtg',
					name: 'RTG電子',
					component: () => import('@/components/views/SlotRTG')
				},
				{
					path: 'slot/ps',
					name: 'PS電子',
					component: () => import('@/components/views/SlotPS')
				},
				{
				    path: 'lottery',
				    name: '彩票彩球',
				    component: () => import('@/components/views/Lottery')
				},
				{
				    path: 'promotion',
				    name: '優惠活動',
				    component: () => import('@/components/views/Promotion')
				},
				{
				    path: 'pricing',
				    name: '包網',
				    component: () => import('@/components/views/Pricing')
				},
						/*
				{
				    path: '/login',
				    name: '登錄頁',
				    component: () => import('@/components/Login')
				},*/
				{
				    path: 'register',
				    name: '註冊頁',
				    component: () => import('@/components/views/Register')
				},
				/*
				{
				    path: 'register',
				    name: '註冊頁',
				    component: () => import('@/components/views/RegisterQuick')
				},
				*/
				{
				    path: 'forget',
				    name: '忘記密碼',
				    component: () => import('@/components/views/Forget')
				},
				{
				    path: 'blog/:id?',
				    component: () => import('@/components/views/Blog')
				},
			]
		},
		{
			path: '/help',
			name: 'HelpLayout',
			component: HelpLayout,
			children: [
				{
				    path: 'about',
				    name: '關於我們',
				    component: () => import('@/components/views/help/About')
				},
				{
				    path: 'recharge',
				    name: '儲值教學',
				    component: () => import('@/components/views/help/Recharge')
				},
				{
				    path: 'trust',
				    name: '責任博彩',
				    component: () => import('@/components/views/help/Trust')
				},
				{
				    path: 'game',
				    name: '遊戲幫助',
				    component: () => import('@/components/views/help/Game')
				},
				{
				    path: 'privacy',
				    name: '隱私保護',
				    component: () => import('@/components/views/help/Privacy')
				},
				{
				    path: 'rule',
				    name: '規則條款',
				    component: () => import('@/components/views/help/Rule')
				},
				{
				    path: 'agent',
				    name: '合營代理',
				    component: () => import('@/components/views/help/Agent')
				},
				{
				    path: 'open',
				    name: '開戶條約',
				    component: () => import('@/components/views/help/Open')
				},
				{
				    path: 'cate1/:id?',
					name: '登錄註冊',
				    component: () => import('@/components/views/help/Cate1')
				},
				{
				    path: 'cate2/:id?',
				    name: '存款流程',
				    component: () => import('@/components/views/help/Cate2')
				},
				{
				    path: 'cate3/:id?',
				    name: '提款流程',
				    component: () => import('@/components/views/help/Cate3')
				},
				{
				    path: 'cate4/:id?',
				    name: '轉帳流程',
				    component: () => import('@/components/views/help/Cate4')
				},
				{
				    path: 'cate5/:id?',
				    name: '帳戶安全',
				    component: () => import('@/components/views/help/Cate5')
				},
				{
				    path: 'cate6/:id?',
				    name: '會員制度',
				    component: () => import('@/components/views/help/Cate6')
				},
				{
				    path: 'cate7/:id?',
				    name: '銀行相關',
				    component: () => import('@/components/views/help/Cate7')
				},
				{
				    path: 'cate8/:id?',
				    name: '其它問題',
				    component: () => import('@/components/views/help/Cate8')
				},
			]
		},
	]
})
router.beforeEach((to, from, next) => {
	if (to.name){
		document.title = to.name;
	}
	axios.get(router.app.$options.store.getters.config.ApiUrl + '/static/version.json', {})
		.then(response => {
			if (router.app.$options.store.getters.version !== 0 && router.app.$options.store.getters.version !== undefined){
				if(router.app.$options.store.getters.version !== response.data.version) {
					location.reload();
				}
			}
			router.app.$options.store.getters.version = response.data.version;
		}).catch(error => {
		})

	if (to.path === "/register" || to.path === "/forget") {
		next();
	}
	else {
		if (!router.app.$options.store.getters.user.isLogin){
			return next({path: "/register"});
		}else {
			next();
		}
	}

})
export default router;
