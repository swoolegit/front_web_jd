// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'

import ElementUI from 'element-ui'
//import 'element-ui/lib/theme-chalk/index.css'
import '../theme/dark/index.css'


//import App from './App'
import router from './router'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

import VueAwesomeSwiper from 'vue-awesome-swiper'

// require styles
import 'swiper/dist/css/swiper.css'
Vue.use(VueAwesomeSwiper, /* { default global options } */)

import store from './store'

import Axios from 'axios'
Vue.prototype.$axios = Axios;
import VueLazyload from 'vue-lazyload'

import GlobalApp from '@/GlobalApp'

Vue.prototype.global = Axios;

import layer from 'vue-layer'

Vue.prototype.$layer = layer(Vue,{
    msgtime: 2,//目前只有一项，即msg方法的默认消失时间，单位：秒
});


Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(ElementUI)
Vue.component('icon', Icon)
Vue.use(VueLazyload,{
	lazyComponent: true
})



import BetPlugin from '@/BetPlugin'

Vue.use(BetPlugin)

import './assets/index.css'
//會員區的style
import './assets/user.css'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
 // components: { App },
  template: '<router-view></router-view>',

   created(){

		//this.$layer.msg('登錄成功');
		//this.$router.push('/lottery');
		//this.$bet.checkLogin();
		this.$store.dispatch('checkLogin');
		/*
		this.$store.dispatch('asyncCheckLogin').then(function (data){
			console.log(data);
		});*/


		//let self = this;
		//console.log(this.$store);
		/*
		Object.keys(this.config.vendors).forEach(function(key, value) {
			self.$set(self.user.wallet.vendorAmounts, key, 0);
			self.$set(self.user.wallet.vendorPercents, key, 0);
			self.refreshVendorsBalance(key);
		});*/
		//console.log(this.$bet);


	},
	 mounted(){

	},
})
