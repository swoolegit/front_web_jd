export const globalMixin = {

	created(){

		this.$axios.interceptors.response.use((response) => {
			//console.log('這是',response.data.status);
			if(response.data.status == 404 && !this.user.error) {

	

				this.user.error = true;
				//this.$store.dispatch('getUserInfo');
				//this.$store.dispatch('checkLogin');
				
				//alert(404);

				this.$alert('目前您的連線已失效，請重新登錄', '提示', {
					confirmButtonText: '確定',
					//cancelButtonText: '取消',
					type: 'warning',
					closeOnClickModal : false,
					showClose : false
				}).then(() => {
					this.user.error = false;
					//alert(11);
					if (/^\/user/.test(this.$router.currentRoute.path)){
						this.$router.replace('/');
					}
					window.location.reload();
					/*
					setTimeout(() => {
						this.LoginBox({methods:'login'});
					}, 100);*/
				}).catch(() => {
				
				});		

				//this.$router.replace('/');
				//return this.$layer.msg('您已變成未登錄狀態');
				this.user.isLogin = false;
				this.user.wallet.refresh = false;
				this.user.wallet.totalAmounts = 0;
				//return this.LoginBox({methods:'login'});
			}
			else {
				return response;
			}
		},(error) => {
	

			if ((error.response.status == 429 || error.response.status == 503) && !this.user.error){
				this.user.error = true;
				this.$alert('操作過於頻繁，請稍候再試', '偵測異常', {
					confirmButtonText: '確定',
					//cancelButtonText: '取消',
					type: 'warning',
					closeOnClickModal : false,
					showClose : false
				}).then(() => {
					this.user.error = false;
				}).catch(() => {
				
				});		

			}

		});

	}
}